#!/usr/bin/env sh

# players
cut -d, -f2  < elo.csv | sort | uniq > players
for p in $(cat players)
do
    cut -d, -f2-3 <  elo.csv | grep "$p" | sort -t, -k2 | tail -n 1
done
