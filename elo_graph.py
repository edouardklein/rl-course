import pandas as pd
import seaborn as sns
import datetime
import matplotlib.pyplot as plt
sns.set()
data = pd.read_csv('elo.csv', header=None, names=["date", "player", "elo"])
data['ts'] = data['date'].apply(
    lambda x: datetime.datetime.fromisoformat(x).timestamp())
g = sns.lineplot(data=data, x='ts', y="elo", hue="player")
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0)
plt.subplots_adjust(right=.5)
plt.savefig("elo.png")
