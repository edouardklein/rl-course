(define-module (rl-course)
  #:use-module (guix packages)
  #:use-module (guix profiles)
  #:use-module (guix git-download)
  #:use-module (guix build-system python)
  #:use-module (gnu packages)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-web)
  #:use-module ((guix licenses) #:prefix license:)

  )

;; (define-public python-bl-rl-course
;;   (package
;;     (name "bl-rl-course")
;;     (version "0.0.0")
;;     (source
;;      (origin
;;        (method git-fetch)
;;        (uri (git-reference
;;              (url "https://gitlab.com/edouardklein/rl-course.git")
;;              (commit version)))
;;        (file-name (git-file-name name version))
;;        (sha256
;;         (base32 "1d1kv13b8kp2xa3iyq08p9jl4cl22qpj0jqxw87jyjknn5lj5hpj"))))
;;     (inputs
;;      `(("ptpython" ,ptpython)
;;        ("python-seaborn" ,python-seaborn)
;;        ("python-ipython" ,python-ipython)
;;        ("python-flask" ,python-flask)
;;        ("python-numpy" ,python-numpy)))

;;     (build-system python-build-system)
;;     (synopsis "The code for a course on Reinforcement Learning")
;;     (description
;;      "The course uses the game of Connect 4 to illustrate Reinforcement Learning.")
;;     (home-page "https://gitlab.com/edouardklein/rl-course")
;;     (license license:agpl3+)))


(packages->manifest
 (map specification->package
      (list "python"
            "python-seaborn"
            "python-ipython"
            "python-flask"
            "python-numpy"
            "python-tqdm"
            "python-scikit-learn")))
