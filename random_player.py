#!/usr/bin/env python3

import io
import numpy as np
import requests
import random

server_url = 'https://api1.rdklein.fr'
board_endpt = server_url + '/board'
move_endpt = server_url + '/move'

data = {
    "name": "random_player",
    "secret": open("random_player.secret").read().strip(),
}

while True:
    r = requests.get(board_endpt, data=data)
    print(r.content.decode())
    board = np.loadtxt(io.StringIO(r.content.decode()), delimiter='\t')
    print(board)
    data["move"] = random.choice(range(7))
    r = requests.post(move_endpt, data=data)
    try:
        board = np.loadtxt(io.StringIO(r.content.decode()), delimiter='\t')
    except:
        with open("error.html", "w") as f:
            f.write(r.content.decode())
    print(board)
