#!/usr/bin/env python3

#!/usr/bin/env python3

import io
import numpy as np
import requests
import random
import server

server_url = 'https://api1.rdklein.fr'
board_endpt = server_url + '/board'
move_endpt = server_url + '/move'

data = {
    "name": "minimax1",
    "secret": 'ldkjfldskjsdlkjf234234',
}

while True:
    r = requests.get(board_endpt, data=data)
    board = np.loadtxt(io.StringIO(r.content.decode()), delimiter='\t')
    sign = 1 if np.sum(board) == 0 else -1
    minimax1 = minimax_player(1)
    print(f"Playing as player {sign}")
    while not win(board):
        data["move"] = minimax1(board, sign)
        r = requests.post(move_endpt, data=data)
        try:
            board = np.loadtxt(io.StringIO(r.content.decode()), delimiter='\t')
            print(board)
        except:
            print(r.content.decode())
            break
