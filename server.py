import numpy as np
import random
from flask import Flask, request, redirect, send_file, send_from_directory
import io
import threading
import pandas as pd
import seaborn as sns
import datetime
import matplotlib.pyplot as plt
import time
import os
from jinja2 import Environment, FileSystemLoader, select_autoescape
env = Environment(
    loader=FileSystemLoader('.'),
    autoescape=select_autoescape(['html', 'xml'])
)
sns.set()

# Connect4
def new_board():
    """Return an empty board"""
    return np.zeros((6, 7), dtype='int8')


def move(board, col, player):
    """Return a copy of board after having played the given move"""
    assert player == 1 or player == -1, f"Invalid player {player}"
    assert 0 <= col < 7, f"Invalid move {col}"
    assert board[0, col] == 0, f"Col {col} is already full"
    answer = np.copy(board)
    i = 5
    while answer[i, col] != 0:
        i -= 1
    answer[i, col] = player
    return answer


def four_in_array(a):
    """Return True if there are 4 in a row in the given array"""
    i = 0
    while 4+i <= len(a):
        if all(a[i:4+i]):
            return True
        i += 1
    return False


def four_in_col(board, j):
    """Return True if there are 4 in a row in the given column"""
    return four_in_array(board[:, j])


def four_in_row(board, i):
    """Return True if there are 4 in a row in the given line"""
    return four_in_array(board[i, :])


def four_in_diag(board, k):
    """Return True if there are 4 in the given diagonal"""
    return four_in_array(np.diagonal(board, offset=k))


def four_in_any(board):
    """Return True if there are 4 in line, row or diag"""
    for i in range(board.shape[0]):
        if four_in_row(board, i):
            return True
    for j in range(board.shape[1]):
        if four_in_col(board, j):
            return True
    for k in range(np.max(board.shape)):
        if four_in_diag(board, k):
            return True
    for k in range(np.max(board.shape)):
        if four_in_diag(np.fliplr(board), k):
            return True
    for k in range(np.max(board.shape)):
        if four_in_diag(board, -k):
            return True
    for k in range(np.max(board.shape)):
        if four_in_diag(np.fliplr(board), -k):
            return True
    return False


def win(board):
    """Return the player if any player has won the game, else False

    If an illegal board (e.g. with two winning players) is given,
    one of them will be arbitrarily returned."""
    if four_in_any(board < 0):
        return -1
    if four_in_any(board > 0):
        return 1
    return False


def legal_moves(board, player):
    """Generate all the legal moves and subsequent boards 1 ply away
    from the given board"""
    for col in range(board.shape[1]):
        try:
            yield col, move(board, col, player)
        except AssertionError:  # Illegal move
            pass


# Minimax
def Q_minimax(depth, board, col, turn, client):
    """Return the state, action value of board, move, from the point of view
    of player client, generating the whole game tree to the specified depth.
    """
    # Assume move is a legal move from board.
    next_board = move(board, col, turn)
    r = win(next_board)
    # At depth 0 we just take the move and evaluate the resulting board.
    # Multiplying by client make it so the number is positive if win(...) ==
    # client, negative otherwise (remember that both factors are either -1 or
    # 1)
    # We also stop the iteration if the reward is non-null, as it marks the
    # end of an episode.
    if r or depth == 0:
        return r*client
    # At any other depths we call ourselves with a decreased depth, for every
    # legal move, in order to evaluate the resulting positions
    next_values = [0] + [Q_minimax(depth-1, next_board, c, turn*-1, client)
                         for c, _ in legal_moves(next_board, turn*-1)]
    if turn*-1 == client:
        # When the time comes to chose among the legal moves from next_board,
        # the client will chose the move that gives the best value.
        # Therefore the value of (board, col) is the best (max) value
        # of all the values we just computed
        return np.max(next_values)
    if turn*-1 != client:
        # When the time comes for the opponent of client to chose a legal move
        # from next_board, they will (or may, it doesn't matter we are
        # preparing for the worst case scenario) chose the move that is the
        # worst (min) for the client. The value of (board, col) is therefore
        # the min value of all the values we just computed
        return np.min(next_values)


# Competition
def random_player(board, sign):
    """Player choosing a legal move at random"""
    return random.choice([(m, b) for m, b in legal_moves(board, sign)])


def Q_player(Q):
    """Return the greedy player with respect to the given state action value
    function"""
    def player(board, sign):
        best_moves = []
        best_score = -np.Inf
        for col, next_board in legal_moves(board, sign):
            score = Q(board, col, sign)
            if score == best_score:
                best_moves += [(col, next_board)]
            elif score > best_score:
                best_moves = [(col, next_board)]
                best_score = score
        return random.choice(best_moves)
    return player


def minimax_player(depth):
    """Return the minimax player of the specified depth"""
    def Q(board, col, sign):
        return Q_minimax(depth, board, col, sign, sign)
    return Q_player(Q)


def game(player_pos, player_neg):
    """Run a game using the provided players.

    Generates a series of pseudo-transitions (p, s, a, s', r)
    with p the player that chooses a.

    To get transitions amendable to RL training, one should concatenate
    every other transition with the one before. i.e.:
    (p, s, a, s', r)
    (-p, s', a, s'', r')
    becomes (s, a, s'', r+r'), which is the RL transition from the point
    of view of player p.

    The transitions from the point of view of player -p can be generated with
    the same method, offset by 1, and paying attention to the sign of the
    rewards.

    """
    board = new_board()
    stop = win(board)
    player = -1
    players = {-1: player_neg, 1: player_pos}
    while not stop:
        player *= -1
        try:
            move, next_board = players[player](board, player)
        except Exception as e:  # Raising is conceding unless there is no legal
            # move, in which case it is a draw
            if list(legal_moves(board, player)) == []:
                yield (player, board, None, board, 0)
            else:
                print(f"Player {player} is conceding: {e}")
                yield (player, board, None, board, -player)
            return
        if win(next_board):
            # Assume here win(next_board) == player, how could
            # player make his opponent win by playing a move
            # in connect four ?
            yield (player, board, move, next_board, player)
            return
        yield (player, board, move, next_board, 0)
        board = next_board


#for p, s, a, next_s, r in game(random_player, random_player):
#    print(p, s, a, next_s, r)
#    input()
# results = []
# for i in range(100):
#     results += [list(game(random_player, random_player))[-1][-1]]
# [sum(x==y for x in results) for y in [-1, 0, 1]]


# elo
def elo_update(A, B, outcome):
    A = A if '/' not in A else A.split('/')[0]
    B = B if '/' not in B else B.split('/')[0]
    K = 32
    R_A = elo[A]
    R_B = elo[B]
    Q_A = pow(10, R_A/400)
    Q_B = pow(10, R_B/400)
    E_A = Q_A/(Q_A+Q_B)
    E_B = Q_B/(Q_A+Q_B)
    new_R_A = R_A + K*((1 if outcome == 1 else 0) - E_A)
    new_R_B = R_B + K*((1 if outcome == -1 else 0) - E_B)
    now = datetime.datetime.now()
    print(f"""Elo update: {A} -> {new_R_A}
    {B} -> {new_R_B}""")
    with open("elo.csv", 'a') as f:
        f.write(f'{now.isoformat()},{A},{new_R_A}\n')
        f.write(f'{now.isoformat()},{B},{new_R_B}\n')
    draw_elo()
    elo[A] = new_R_A
    elo[B] = new_R_B


def draw_elo():
    """Make a graph of the elo"""
    plt.figure()
    data = pd.read_csv('elo.csv', header=None,
                       names=["date", "player", "elo"])
    data['ts'] = data['date'].apply(
        lambda x: datetime.datetime.fromisoformat(x).timestamp())
    g = sns.lineplot(data=data, x='ts', y="elo", hue="player", style="player", dashes=True)
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0)
    plt.subplots_adjust(right=.5)
    plt.savefig("elo.png")


# Server
app = Flask(__name__)
AI_players = ['random_player',
              'minimax_player(0)',
              'minimax_player(1)',
              'minimax_player(2)',
              'minimax_player(3)',
              'minimax_player(4)',
              #'minimax_player(5)',
              #'minimax_player(6)',
              #'minimax_player(7)',
              #'minimax_player(8)',
              ]
elo = {p: 1500 for p in AI_players}
lobby = []
matches = []


def color(c):
    """Return the color of cell value c"""
    return {-1: "red", 0: "white", 1: "blue"}[c]


def board2html(board):
    """Return a HTML representation of the given board"""
    answer = ""
    for line in board:
        answer += '<tr>'
        for cell in line:
            answer += f'<td style="background-color: {color(cell)};"></td>'
        answer += '</tr>'
    return answer


class Match:
    """Holds the information about a game between two players"""

    def __init__(self, p1, p2):
        self.players = {1: p1, -1: p2}
        self.board = new_board()
        self.turn = -1
        self.timeout = 30
        self.winner = False
        self.now = None
        self.next_move()

    def next_move(self):
        """Wait for next move"""
        if win(self.board):
            self.winner = win(self.board)
            self.end()
            return
        self.now = datetime.datetime.now()
        self.turn *= -1
        if '/' in self.players[self.turn]:  # AI player ?
            t = threading.Thread(target=self.AI_play)
            t.start()

    def AI_play(self):
        """Request a move from the AI whose turn it is"""
        AI_player = eval(self.players[self.turn].split("/")[0],
                         globals())
        move, new_board = AI_player(self.board, self.turn)
        if not self.check_timeout():
            self.board = new_board
            self.next_move()

    def remote_play(self, name, col):
        """Register a move received by HTTP"""
        assert self.players[self.turn] == name
        new_board = move(self.board, col, self.turn)
        if not self.check_timeout():
            self.board = new_board
            self.next_move()

    def forfeit(self, name):
        """Forfeil game for player name"""
        player = [k for k in self.players if self.players[k] == name][0]
        self.winner = player*-1
        self.end()

    def check_timeout(self):
        """Control timeout and forfeit the slow player"""
        if self.now is None:
            return False
        now = datetime.datetime.now()
        if (now - self.now).total_seconds() > self.timeout:
            self.winner = self.turn*-1  # Forfeit game on timeout
            self.end()
            return True
        return False

    def end(self):
        """Update elo"""
        elo_update(self.players[1], self.players[-1], self.winner)

    def to_html(self, name):
        player = [k for k in self.players if self.players[k] == name][0]
        opponent = -player
        player_name = name
        opponent_name = self.players[opponent]
        return env.get_template("board.html").render(
            name=player_name,
            opponent=opponent_name,
            turn=self.turn == player,
            opponent_color=color(opponent),
            player_color=color(player),
            sign=player,
            cols=self.board.shape[1],
            winner=self.winner or win(self.board),
            winner_color=color(win(self.board)),
            board=board2html(self.board)
        )


lock = threading.Lock()


def handle_games(auto_lobby=False):
    """Check the timeout on games, and maybe return players to the lobby (off by
    default)"""
    global matches
    global lobby
    global lock
    while True:
        time.sleep(1)
        with lock:
            for match in matches:
                match.check_timeout()
            finished_matches = [m for m in matches if m.winner]
            ongoing_matches = [m for m in matches if not m.winner]
            for m in finished_matches:
                for player in m.players.values():
                    if '/' not in player and auto_lobby:  # Human player
                        lobby += [player]
            matches = ongoing_matches


t = threading.Thread(target=handle_games)
t.start()


lobby_timeout = 10


def handle_lobby(server_ai=False):
    global lobby_timeout
    global matches
    global lobby
    global lock
    while True:
        time.sleep(1)
        with lock:
            lobby_timeout -= 1
            if lobby_timeout <= 0:
                lobby_timeout = 10
                lobby = sorted(lobby, key=lambda x: random.random())
                while len(lobby) >= 2:
                    match = Match(lobby.pop(), lobby.pop())
                    matches += [match]
                if lobby and server_ai:
                    AI_player = random.choice(AI_players) + f"/{random.randint(0, 10000)}"
                    if random.random() >= .5:
                        match = Match(lobby.pop(), AI_player)
                    else:
                        match = Match(AI_player, lobby.pop())
                    matches += [match]


t = threading.Thread(target=handle_lobby)
t.start()

# Each secret is stored in a file bearing the name of the authorized user.
# Upon first login, this secret is revealed to the user, and then the user must keep
# providing the secret on subsequent requests
# Reinitialization of the secret must be done by hand by creating a new secret file server side
def check_secret(form_data):
    """Raise an exception if the request does not contain the appropriate secret."""
    with open(f'{form_data["name"]}.secret', 'r') as f:
        assert f.read().strip() == form_data['secret']

def reveal_secret(form_data):
    """Return the secret for the unlogged user in the request. This is supposed
    to work only once."""
    with open(f'{form_data["name"]}.unrevealedsecret', 'r') as f:
        secret = f.read().strip()
    os.rename(f'{form_data["name"]}.unrevealedsecret', f'{form_data["name"]}.secret')
    return secret

@app.route('/login', methods=['POST'])
def api_login():
    """Reveal the secret to the player if the player does not know their secret yet"""
    return reveal_secret(request.form)

def match_for_player(name):
    """Return the match in which the given player is playing"""
    _matches = [match for match in matches
                if name in match.players.values()]
    assert len(_matches) <=1, f"Player {name} is in more than 1 match"
    if len(_matches) == 0:
        return None
    return _matches[0]

def board_to_text(board):
    """Return a textual representation of the board"""
    # Create a virtual file object
    vfile = io.StringIO()
    # Save the array to the virtual file object
    np.savetxt(vfile, board, delimiter='\t', fmt='%d')
    # Retrieve the string from the virtual file object
    vfile.seek(0) # Go to the start of the StringIO object
    return vfile.read()

@app.route('/board')
def api_board():
    """Block until we can return a board to the user"""
    global lobby
    check_secret(request.form)
    name = request.form["name"]
    if name not in elo:
        elo[name] = 1500
        lobby += [name]
    with lock:
        m = match_for_player(name)
        if name not in lobby and m is None:
            lobby += [name]
    while m is None:
        time.sleep(1)
        m = match_for_player(name)
    # At this point we have the match in which name is playing But this match
    # may be over Now we wait for it to be the turn of the requesting player but
    # we check that the match is not somehow over (e.g. won by the opponent)
    display_board = False
    while not display_board:
        # It's name's turn
        display_board = m.players[m.turn] == name
        # or it's over
        display_board = display_board or m.winner
    return board_to_text(m.board)

@app.route('/move', methods=['POST'])
def api_move():
    """Register the move in the match"""
    check_secret(request.form)
    name = request.form['name']
    move = int(request.form['move'])
    match = match_for_player(name)
    match.remote_play(name, move)
    return board_to_text(match.board)

def http_move():
    """Register the move in the appropriate queue and display the next board"""
    name = request.form['name']
    move = int(request.form['move'])
    match = [m for m in matches if name in m.players.values()][0]
    match.remote_play(name, move)
    return redirect('/', code=307)


@app.route("/lobby/<name>")
def lobby2html(name):
    names_elos = [f"{n} ({elo[n]})" for n in lobby
                  if n != name]
    name_elo = f"{name} ({elo[name]})"
    return env.get_template("lobby.html").render(
        name=name,
        t=lobby_timeout,
        user=name_elo,
        l=names_elos,
        )

#@app.route('/', methods=['POST'])
def new_game():
    global elo
    global lobby
    name = request.form["name"]
    if not name.endswith("@epita.fr"):
        raise ValueError("Invalid Email")
    if name not in elo:
        elo[name] = 1500
        lobby += [name]
    if name in lobby:
        return lobby2html(name)
    if any(name in match.players.values() for match in matches):
        match = [match for match in matches
                 if name in match.players.values()][0]
        return match.to_html(name)
    lobby += [name]
    return lobby2html(name)


#@app.route('/', methods=['GET'])
def login():
    return open("login.html").read()

#@app.route('/logout', methods=['POST'])
def logout():
    global lock
    global lobby
    name = request.form["name"]
    with lock:
        if name in lobby:
            lobby.remove(name)
            return redirect('/', code=302)
        match = [m for m in matches if name in m.players.values()][0]
        match.forfeit(name)
    while True:
        time.sleep(1)
        with lock:
            if name not in lobby:
                continue
            lobby.remove(name)
            return redirect('/', code=302)

@app.route('/elo')
def elo_graph_html():
    return env.get_template("elo.html").render()

@app.route('/elo.png')
def elo_route():
    """Serves the elo graph"""

    with open("elo.png", 'rb') as bites:
        return send_file(
                     io.BytesIO(bites.read()),
                     attachment_filename='logo.jpeg',
                     mimetype='image/png'
               )
if __name__ == '__main__':
    app.run()
# RL

def one_hot_encode_action(a):
    "Return the one-hot encoded vector for action a"
    answer = np.zeros(7, dtype='int8')
    answer[a] = 1
    return answer

def naive_psi(board, action):
    """Reshape a state, action couple into a 7*6+7-dimensional vector.

    Just reshape the board and one-hot encode the action, nothing fancy."""
    return np.concatenate([board.reshape(-1),
                           one_hot_encode_action(action)])

def extract_transitions(g, player):
    if player == -1:
        g = g[1:]
    if len(g) % 2 == 1:
        g = g + [[None, None, None, g[-1][3], 0]]
    return [(player*pos_pt[1], pos_pt[2], player*neg_pt[3],
             player*(pos_pt[4]+neg_pt[4]))
            for pos_pt, neg_pt in zip(g[::2], g[1::2])]

def q_learning(SA, R, S2, value_from_quality, regressor, gamma=.9):
    """Return a trained sklearn object according to the Q-learning algorithm

    value_from_quality must be a function that, given a state-action Quality function
    from R^n to R, returns a state Value function from state to R."""
    regressor.fit(SA, np.zeros(SA.shape[0]))
    for alpha in np.arange(1, 0, -.1):
        V = value_from_quality(regressor)
        Y = (1-alpha)*regressor.predict(SA) + alpha*(R + gamma*V(S2))
        regressor.fit(SA, Y)
    return regressor

def connect4_value_from_quality(psi):
    """Return a value_from_quality function that eats a Q_regressor, and outputs
    a state value function"""
    def answer(Q_regressor):
        """Return a state-value function from a state-action quality function"""
        def value_function(state_list):
            """Return the values of the provided states"""
            TRANSITION_AXIS=0
            ACTION_AXIS=1
            Q_action_values = np.zeros((len(state_list), 7))
            for a in range(7):
                Q_action_values[:,a] = Q_regressor.predict(
                    np.array([psi(s, a) for s in state_list]))
            return np.max(Q_action_values, axis=ACTION_AXIS)
        return value_function
    return answer

def Q_from_regressor(psi, regressor):
    """Return a state-action value from a regressor, typically a pickled trained
    regressor from a previous run
    """
    def board_col_value_function(board, col):
        """Get the value of the requested board (==state), col (==action)
        from the trained sklearn object"""
        return regressor.predict([psi(board, col)])
    def answer(board, col, sign):
        """Call the actual state-action value function with
        a board where it's player 1's turn to play"""
        return board_col_value_function(sign*board, col)
    return answer

def connect4_q_learning(transitions, psi, regressor):
    """Return the state-action value function, suitable for use with Q_player"""
    Q_regressor = q_learning(SA=np.array([psi(t[0], t[1]) for t in transitions]),
                             R=np.array([t[-1] for t in transitions]),
                             S2=[t[2] for t in transitions],
                             value_from_quality=connect4_value_from_quality(psi),
                             regressor=regressor)
    return Q_from_regressor(psi, Q_regressor)

@app.route('/pub/<path:filename>')
def serve_file(filename):
    return send_from_directory('pub', filename)
