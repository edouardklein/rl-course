#!/usr/bin/env python3
import io
import numpy as np
import requests
import random
from server import *
import sys

server_url = 'https://api1.rdklein.fr'
board_endpt = server_url + '/board'
move_endpt = server_url + '/move'

data = {
    "name": "minimax"+sys.argv[1],
    "secret": open(f'minimax{sys.argv[1]}.secret').read().strip(),
}

player = minimax_player(1)


while True:
    r = requests.get(board_endpt, data=data)
    board = np.loadtxt(io.StringIO(r.content.decode()), delimiter='\t')
    print(board)
    if np.sum(board) == 0:
        sign = 1
    elif np.sum(board) == 1:
        sign = -1
    try:
        move, _ = player(board, sign)
    except IndexError:
        print("No valid moves")
        continue
    data["move"] = move
    r = requests.post(move_endpt, data=data)
    try:
        board = np.loadtxt(io.StringIO(r.content.decode()), delimiter='\t')
    except:
        print(r.content.decode())
    print(board)
