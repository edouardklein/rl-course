#!/usr/bin/env python3
import io
import numpy as np
import requests
import random
from server import *
from sklearn import svm

server_url = 'https://edk.the-dam.org'
board_endpt = server_url + '/board'
move_endpt = server_url + '/move'

data = {
    "name": "moyenne",
    "secret": "danstoncullabalayette",
}

g = list(game(random_player, random_player))
transitions = extract_transitions(g, 1) + extract_transitions(g, -1)
Q = connect4_q_learning(transitions, naive_psi, svm.SVR())


player = Q_player(Q)


while True:
    r = requests.get(board_endpt, data=data)
    board = np.loadtxt(io.StringIO(r.content.decode()), delimiter='\t')
    print(board)
    if np.sum(board) == 0:
        sign = 1
    elif np.sum(board) == 1:
        sign = -1
    move, _ = player(board, sign)
    data["move"] = move
    r = requests.post(move_endpt, data=data)
    try:
        board = np.loadtxt(io.StringIO(r.content.decode()), delimiter='\t')
    except:
        with open("error.html", "w") as f:
            f.write(r.content.decode())
    print(board)
